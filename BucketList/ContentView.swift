//
//  ContentView.swift
//  BucketList
//
//  Created by Pascal Hintze on 13.03.2024.
//

import MapKit
import SwiftUI

struct ContentView: View {
    let startPosition = MapCameraPosition.region(MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: 56, longitude: -3),
        span: MKCoordinateSpan(latitudeDelta: 10, longitudeDelta: 10)))

    @State private var viewModel = ViewModel()

    @State private var mapStyle: MapStyle = .standard
    @State private var defaultMapStyle = true

    var body: some View {
        if viewModel.isUnlocked {
            MapReader { proxy in
                Map(initialPosition: startPosition) {
                    ForEach(viewModel.locations) { location in
                        Annotation(location.name, coordinate: location.coordinate) {
                            Image(systemName: "star.circle")
                                .resizable()
                                .foregroundStyle(.yellow)
                                .frame(width: 44, height: 44)
                                .background(.white)
                                .clipShape(.circle)
                                .onLongPressGesture {
                                    viewModel.selectedPlace = location
                                }
                        }
                    }
                }
                .onTapGesture { position in
                    if let coordinate = proxy.convert(position, from: .local) {
                        viewModel.addLocation(at: coordinate)
                    }
                }
                .sheet(item: $viewModel.selectedPlace) { place in
                    EditView(location: place) {
                        viewModel.update(location: $0)
                    }
                }
                .mapStyle(mapStyle)

                Button("Change Map Mode") {
                    defaultMapStyle.toggle()
                    mapStyle = defaultMapStyle ? MapStyle.standard : MapStyle.hybrid
                }
            }
        } else {
            Button("Unlock Places", action: viewModel.authenticate)
                .padding()
                .background(.blue)
                .foregroundStyle(.white)
                .clipShape(.capsule)
                .alert("Authentication Error", isPresented: $viewModel.hasError) {
                    Button("OK") { }
                } message: {
                    Text(viewModel.errorMessage ?? "Unknown Error")

                }
        }

    }
}

#Preview {
    ContentView()
}
