# BucketList

BucketList is an app that lets the user build a private list of places on a map that they intend to visit one day.

This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/bucket-list-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- Comparable
- User’s documents directory
- Embed maps into a SwiftUI app
- How to use Face ID to unlock your app
- Integrating MapKit with SwiftUI
- Network calls
- Add Comparable conformance
- Write data to disk securely
- How to enable biometric authentication